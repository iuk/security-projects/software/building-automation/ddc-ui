import logging

from lib.routes import app

if __name__ == '__main__':
    log = logging.getLogger('werkzeug')
    #log.setLevel(logging.ERROR)
    app.run(debug=True)
