from gunicorn.http import wsgi
from gunicorn import util

# Liste der Header die nicht von der Anwendung geschrieben werden dürfen
from gunicorn.workers.base import Worker

util.hop_headers = set()


def handle_error(self, req, client, addr, exc):
    pass


def default_headers(self):
    # Macht aus "NOT FOUND" das etwas nettere "Not Found":
    status = self.status.title()

    # Die Header müssen sein, alle anderen macht die Anwendung selbst
    headers = [
        "HTTP/%s.%s %s\r\n" % (self.req.version[0],
                               self.req.version[1], status),
        "Allow: GET\r\n",
    ]
    if self.chunked:
        headers.append("Transfer-Encoding: chunked\r\n")
    return headers


wsgi.Response.default_headers = default_headers
Worker.handle_error = handle_error