#!/usr/bin/env bash
cd $(dirname $0)
PATH=~/.local/bin:/home/pi/.pyenv/shims/:$PATH
gunicorn --bind=0.0.0.0 -c config.py --threads 4 app:app
