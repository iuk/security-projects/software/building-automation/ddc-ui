import random
import time
from io import BytesIO
from queue import Queue

from jinja2 import Template
from flask import Flask, send_from_directory, request, send_file

from lib.ui import UI

app = Flask(__name__)
uis = {}

action_q = Queue()
action_q.put(1)


def get_ui(cid, ip):
    if cid not in uis:
        uis[cid] = UI(cid, ip)
    return uis[cid]


def load_html(fn, **kwargs):
    with open("static/{}".format(fn)) as f:
        t = Template(f.read())
    return t.render(**kwargs)


@app.route("/")
def index():
    return load_html("index.html")


@app.route("/check_browser.html")
def check_browser():
    return load_html("check_browser.html", iid=random.randint(0, 2 ** 16))


@app.route("/err_browser.html")
def err_browser():
    return load_html("err_browser.html")


@app.route("/ddcdialog.html")
def ddcdialog():
    return load_html("ddcdialog.html", cid=random.randint(0, 2 ** 16))


@app.route("/ddcdialog_frame.html")
def ddcdialog_frame():
    cid = request.args.get('cid')
    return load_html("ddcdialog_frame.html", cid=cid)


@app.route("/ddcerror.html")
def ddcerror():
    return load_html("ddcerror.html")


@app.route("/click_frame.html")
def click_frame():
    return load_html("click_frame.html")


@app.route("/Images/kphead.jpg")
def kphead():
    return send_from_directory("../static/Images/", "kphead.jpg")


@app.route("/Images/trans.gif")
def trans():
    return send_from_directory("../static/Images/", "trans.gif")


@app.route("/checkperf_0.gif")
def checkperf0():
    return send_from_directory("../static/", "checkperf_0.gif")


@app.route("/checkperf_1.gif")
def checkperf1():
    return send_from_directory("../static/", "checkperf_1.gif")


@app.route("/checkperf_2.gif")
def checkperf2():
    return send_from_directory("../static/", "checkperf_2.gif")


@app.route("/ddcovl_<int:id>.gif")
def ddcovl(id):
    time.sleep(random.randint(1, 5))
    return send_from_directory("../static/", "empty.gif")


@app.route("/ddcmain_<int:id>.gif")
def ddcmain(id):
    try:
        action_q.get(True, 2)
    except:
        pass
    cid = request.args.get('cid')
    img = get_ui(cid, request.remote_addr).getimage()
    if type(img) is BytesIO:
        return send_file(img, mimetype='image/gif')
    return send_from_directory("../pages/", img)


@app.route("/doclick.gif")
def doclick():
    cid = request.args.get('cid')
    get_ui(cid, request.remote_addr).click(request.args.get('x'), request.args.get('y'))
    action_q.put(17+4)
    return send_from_directory("../static/", "empty.gif")


@app.after_request
def apply_headers(response):
    if response.status_code is not 200:
        del(response.headers["Content-Type"])
    else:
        response.headers["Content-Type"] = response.headers["Content-Type"].replace("; charset=utf-8", "")
    response.headers["Cache-Control"] = "no-cache"
    response.headers["Server"] = "hsgUiServer/1.1"
    response.headers["Connection"] = "Keep-Alive"
    response.headers["Keep-Alive"] = "timeout=60"

    return response


@app.errorhandler(404)
def page_not_found(e):
    return b"", 404
