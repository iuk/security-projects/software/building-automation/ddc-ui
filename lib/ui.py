import datetime
import glob
import string
from io import BytesIO

from PIL import ImageFont, ImageDraw
from ruamel.yaml import YAML
from PIL import Image

from lib.util import Logged

yaml = YAML(typ='safe')


def is_falsy(x):
    return x not in [None, False, "No", "False"]


class UI(Logged):
    elements = {}

    def __init__(self, cid, ip):
        super().__init__()
        self.cid = cid
        self.ip = ip
        self.pages = {}
        for fn in glob.glob("./pages/*.yml"):
            with open(fn) as f:
                n = fn.rsplit("/", 1)[1].replace(".yml", "")
                self.pages[n] = yaml.load(f)
        self.current_page = "overview"
        self.current_obj = None
        self.current_params = {"user": -1}
        self.current_image = None

    def __str__(self):
        return "UI(cid={}, ip={})".format(self.cid, self.ip)

    def click(self, x, y):
        p = self.pages[self.current_page]
        x, y = int(x), int(y)
        marked_image = self.get_image_marked(x, y)

        for l in p["links"]:
            if "params" in l:
                self.current_params.update(l["params"])
            if UI.hit(l, x, y):
                self.log(f"Click on ({x}, {y}) in {self.current_page} opens {l['to']}.", image=marked_image)
                self.current_page = l["to"]
                if self.current_page == "after_login":
                    self.current_page = self.current_params["after_login"]
                    self.log(f"after_login page: {self.current_params['after_login']}")
                if "if_loggedin" in self.pages[self.current_page] and "user" in self.current_params and self.current_params["user"] > 0:
                    self.current_page = self.pages[self.current_page]["if_loggedin"]
                self.update_reference()
                return

        self.update_reference()
        if self.current_obj and self.current_obj.click(x, y):
            return

        self.log(f"Click on ({x}, {y}) in {self.current_page} has no action.", image=marked_image)

    def get_image_marked(self, x, y):
        image = self.current_image
        image = image.convert("RGB")
        draw = ImageDraw.Draw(image)

        circle = lambda r, c: draw.ellipse((x - r, y - r, x + r, y + r), fill=c)
        circle(4, (255, 255, 255))
        circle(2, (255, 0, 255))

        img_io = BytesIO()
        image.save(img_io, 'png')
        img_io.seek(0)
        return img_io


    def update_reference(self):
        p = self.pages[self.current_page]
        if "is" in p:
            clazz = globals()[p["is"]]
            if type(self.current_obj) is not clazz:
                self.current_obj = clazz(self)
        else:
            self.current_obj = None

    def getimage(self):
        self.current_params["date"] = datetime.datetime.now().strftime("%d.%m.%y")
        self.current_params["time"] = datetime.datetime.now().strftime("%H:%M")
        p = self.pages[self.current_page]

        self.log(f"Sending image for {self.current_page}")

        if self.current_obj:
            image = self.current_obj.render()
        else:
            image = Image.open("./pages/" + self.current_page + ".png", "r")

        if "overwrite" in p:
            image = image.convert("RGB")
            for o in p["overwrite"]:
                fnt = './pixel_arial.ttf' if not is_falsy(o["bold"]) else './pixel_arial_bold.TTF'
                fnt = ImageFont.truetype(fnt, o["size"])
                text = o["with"].format(**self.current_params)
                w, h = fnt.getsize(text)
                d = ImageDraw.Draw(image)
                d.rectangle([(o["x"], o["y"]), (o["x"] + w, o["y"] + h)], fill=o["background"])
                d.text((o["x"], o["y"]), text, font=fnt, fill=o["foreground"])

        img_io = BytesIO()
        image.save(img_io, 'gif')
        self.current_image = image
        img_io.seek(0)
        return img_io

    @staticmethod
    def hit(link, x, y):
        return link["upper_left"]["x"] < x \
               and link["upper_left"]["y"] < y \
               and link["lower_right"]["x"] > x \
               and link["lower_right"]["y"] > y


class UIElement(object):
    pass


class Keyboard(object):
    def __init__(self, ui):
        self.ui = ui
        super().__init__()
        self.input = ""
        self.charmap_ = string.digits[1:] + "0" + string.ascii_lowercase + "{[]},.-   \0\0\0\0"
        self.charmap_caps = "!\"§$%&/()=" + string.ascii_uppercase + "+><?;:_   \0\0\0\0"
        self.charmap_symbols = "\0\0\0\0\0\0{[]}\0\0\0\0\0\0\0+*~\0\0\0\0\0\0@`#'<>|\0\0\0\0^\0\\\0\0\0   \0\0\0\0"
        self.style = ""

    def __str__(self):
        return "Keyboard({})".format(str(self.ui))

    def render(self):
        keyboard = Image.open("./ui-elements/keyboard-{}.png".format(self.style), "r").convert("RGB")
        star = Image.open("./ui-elements/keyboard-star.png", "r")
        cursor = Image.open("./ui-elements/keyboard-cursor.gif", "r")
        x, y = 43, 39
        for _ in self.input:
            keyboard.paste(star, (x, y))
            x += star.size[0]
        keyboard.paste(cursor, (x, y))
        return keyboard

    def get_char(self, x, y):
        x -= 5
        y -= 94
        per_row = 10
        h = 31
        pos = int(y/h) * per_row + int(x/h)
        return getattr(self, "charmap_{}".format(self.style))[pos]

    def click(self, x, y):
        if 99 < x < 128 and 65 < y < 93:
            self.style = ""
            return True
        if 130 < x < 159 and 65 < y < 93:
            self.style = "caps"
            return True
        if 161 < x < 190 and 65 < y < 93:
            self.style = "symbols"
            return True

        if 222 < x < 251 and 65 < y < 93:
            self.input = ""
            self.ui.log("Keyboard cleared.")
            return True

        if 5 < x < 313 and 97 < y < 236:
            c = self.get_char(x, y)
            if c == "\0":
                self.ui.log(f"Click on ({x}, {y}) has no action.")
                return True
            self.input += c
            self.ui.log("Current keyboard value: {}".format(self.input))
            return True


