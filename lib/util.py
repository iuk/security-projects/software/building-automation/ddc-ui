import base64
import datetime
import errno
import os
import random
import time
from os.path import expanduser

template_header = """
<html>
<style>
.log-message {
    border-style: 1px solid black;
    border-radius: 2vh;
    padding: 5vh;
}
</style>
"""

template_message = """
<div class="log-message"><b>{time}</b><br>{message}<br>
{image}
<p class="info">{name}</p>
</div>
"""


class Logged(object):
    def __init__(self):
        self.mkdir_p(expanduser("~/.ddc-ui/logs/"))
        self.html_file = expanduser("~/.ddc-ui/logs/log_{}.html".format(time.time(), random.randint(0, 2**10)))
        self.fh = open(self.html_file, "w")
        self.fh.write(template_header)

    def log(self, message, image=""):
        print("[{}] {}: {}".format(datetime.datetime.now(), self, message))
        if image:
            enc = base64.b64encode(image.getvalue()).replace(b"'", b"").decode("utf8")
            image = "<img src='data:image/png;base64,{}'>".format(enc)
        msg = template_message.format(time=datetime.datetime.now(), name=self, message=message, image=image)
        self.fh.write(msg)
        self.fh.flush()

    @staticmethod
    def mkdir_p(path):
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise
